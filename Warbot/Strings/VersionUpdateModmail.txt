﻿Hello. I've just updated the patch version numbers. Please review the changes below and verify they are correct.

|Platform|Old Version|New Version|
|:---:|:---|:---|
|**PC**|#{CURRENT_PC} ([link](#{CURRENT_PC_LINK}))|#{NEW_PC} ([link](#{NEW_PC_LINK}))|
|**PS4/XB1**|#{CURRENT_CONSOLE} ([link](#{CURRENT_CONSOLE_LINK}))|#{NEW_CONSOLE} ([link](#{NEW_CONSOLE_LINK}))|

-----

*This is an automated message, and I am just a bot.*