﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warbot
{
    public static class StringExtensions
    {
        public static string PadBoth( this string source, int totalWidth, char padChar )
        {
            if( source.Length >= totalWidth )
                return source;

            int left = (int)Math.Floor( totalWidth / 2d );
            int right = totalWidth - left;

            return source.PadLeft( left, padChar ).PadRight( right, padChar );
        }
    }
}