﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Warbot
{
    public static class ThreadStateExtensions
    {
        public static string Display( this ThreadState state )
        {
            if( state.HasFlag( ThreadState.Background ) )
                state = state ^ ThreadState.Background;

            if( state == ThreadState.WaitSleepJoin )
                return "Wait/sleep/join";
            
            var name = new StringBuilder( state.ToString() );

            for( var i = 1; i < name.Length; ++i )
            {
                if( Char.IsUpper( name[i] ) )
                {
                    name[i] = Char.ToLower( name[i] );
                    name.Insert( i, " " );
                    ++i;
                }
            }

            return name.ToString();
        }
    }
}