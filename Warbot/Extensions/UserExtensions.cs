﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedditSharp;
using RedditSharp.Things;

namespace Warbot
{
    public static class UserExtensions
    {
        public static bool IsModeratorOf( this AuthenticatedUser user, Subreddit sub )
        {
            foreach( var modSubs in user.ModeratorSubreddits )
                if( modSubs.FullName == sub.FullName )
                    return true;

            return false;
        }

        public static bool IsSubscribedTo( this AuthenticatedUser user, Subreddit sub )
        {
            foreach( var subbed in user.SubscribedSubreddits )
                if( subbed.FullName == sub.FullName )
                    return true;

            return false;
        }
    }
}