﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Warbot
{
    public static class CharExtensions
    {
        private static readonly ReadOnlyCollection<char> Vowels;

        static CharExtensions()
        {
            Vowels = new ReadOnlyCollection<char>( new[] { 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' } );
        }

        public static bool IsVowel( this char c )
        {
            return Vowels.Contains( c );
        }
    }
}