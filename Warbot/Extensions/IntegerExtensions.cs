﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Warbot
{
    internal static class IntegerExtensions
    {
        public static int Seconds( this int value )
        {
            return value * 1000;
        }

        public static int Minutes( this int value )
        {
            return value.Seconds() * 60;
        }

        public static int Hours( this int value )
        {
            return value.Minutes() * 60;
        }
    }
}