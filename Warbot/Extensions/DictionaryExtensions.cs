﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warbot
{
    public static class DictionaryExtensions
    {
        public static bool ContainsKey<TV>( this Dictionary<string, TV> dict, string key, bool caseSensitive, out string actual )
        {
            foreach( var pair in dict )
            {
                
                var key1 = caseSensitive ? pair.Key : pair.Key.ToLower();
                var key2 = caseSensitive ? key : key.ToLower();

                if( key1 == key2 )
                {
                    actual = pair.Key;
                    return true;
                }
            }

            actual = null;
            return false;
        }
    }
}