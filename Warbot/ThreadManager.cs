﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.ObjectModel;

namespace Warbot
{
    internal static class ThreadManager
    {
        private struct ThreadInfo
        {
            public bool IsParameterized;
            public object Parameter;
            public Thread Thread;
        }

        private static List<ThreadInfo> ThreadInfos;

        public static ReadOnlyCollection<Thread> Threads
        {
            get
            {
                return ThreadInfos.Select( x => x.Thread )
                    .ToList()
                    .AsReadOnly();
            }
        }

        static ThreadManager()
        {
            ThreadInfos = new List<ThreadInfo>();
        }

        public static void CreateNew( string name, ParameterizedThreadStart callback, object param )
        {
            var info = new ThreadInfo
            {
                IsParameterized = true,
                Parameter = param,
                Thread = new Thread( callback )
            };

            info.Thread.Name = name;
            info.Thread.IsBackground = true;

            ThreadInfos.Add( info );
        }

        public static void StartAll()
        {
            foreach( var info in ThreadInfos )
            {
                if( !info.Thread.ThreadState.HasFlag( ThreadState.Unstarted ) )
                    continue;

                if( info.IsParameterized )
                    info.Thread.Start( info.Parameter );
                else
                    info.Thread.Start();
            }
        }

        public static void SuspendAll()
        {
            foreach( var info in ThreadInfos )
                info.Thread.Suspend();
        }

        public static void ResumeAll()
        {
            foreach( var info in ThreadInfos )
                info.Thread.Resume();
        }

        public static void AbortAll( bool waitForJoin = true )
        {
            foreach( var info in ThreadInfos )
            {
                info.Thread.Abort();

                if( waitForJoin )
                    info.Thread.Join();
            }
        }
    }
}