﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using RedditSharp;
using RedditSharp.Things;
using CommandLine;

namespace Warbot
{
    using Actions;

    internal static class Program
    {
        private static volatile bool Running;
        private static Thread CommandThread;
        private static ReadOnlyCollection<Action> Actions;
        private static object syncRoot;
        private static Reddit reddit;
        private static Subreddit subreddit;
        private static AuthenticatedUser user;

        public static Reddit Reddit
        {
            get { return reddit; }
        }

        public static Subreddit Subreddit
        {
            get { return subreddit; }
        }

        public static AuthenticatedUser User
        {
            get { return user; }
        }

        static Program()
        {
            syncRoot = new object();
            reddit = new Reddit( WebAgent.RateLimitMode.Pace );

            CommandThread = new Thread( CommandLoop );
            CommandThread.IsBackground = false; //Keep the console window open.

            Command.Register( "status", StatusCommand );

            Actions = new ReadOnlyCollection<Action>( new Action[]
            {
                new FilterModQueueAction(),
                new ScanVersionsAction(),
            } );
        }

        private static void Main( string[] args )
        {
            Console.Title = "Warbot";
            WebAgent.UserAgent = "Warbot/1.0 by /u/SirNastyPants";

            var result = Parser.Default.ParseArguments<CliOptions>( args );
            if( result.Errors.Count() > 0 )
            {
                foreach( var err in result.Errors )
                    Console.WriteLine( err.ToString() );

                return;
            }

            var options = result.Value;

            string username = options.Username,
                password = options.Password,
                subname = options.Subreddit;

            if( username == null )
                Prompt( "Username", out username );

            if( password == null )
                Prompt( "Password", out password );

            if( subname == null )
                Prompt( "Subreddit", out subname );

            Console.Clear();

            try
            {
                Console.WriteLine( "Authenticating as /u/{0} on /r/{1}", username, subname );
                user = reddit.LogIn( username, password );
                Console.WriteLine( "Authenticated successfully." );
            }
            catch( Exception ex )
            {
                Console.Error.WriteLine( "Could not authenticate. Reason: {0}", ex.Message );
                Console.Error.Flush();
                Environment.Exit( 1 );
            }
            finally
            {
                //Force password to be deleted from memory asap.
                password = null;
                options.UnsetPassword();
                options = null;
                GC.Collect();
            }

            try
            {
                Console.WriteLine( "Attempting to start bot." );
                subreddit = reddit.GetSubreddit( subname );

                if( !user.IsModeratorOf( subreddit ) )
                {
                    Console.Error.WriteLine( "/u/{0} is not a moderator of /r/{1}", username, subname );
                    Console.Error.Flush();
                    Environment.Exit( 2 );
                }

                if( !user.IsSubscribedTo( subreddit ) )
                {
                    Console.WriteLine( "Subscribing to /r/{0}", subname );
                    subreddit.Subscribe();
                }

                foreach( var action in Actions )
                    ThreadManager.CreateNew( action.Id, ActionLoop, action );

                Console.CancelKeyPress += ( s, e ) => { Running = false; };
                Running = true;

                ThreadManager.StartAll();
                CommandThread.Start();

                Console.WriteLine( "Bot started successfully. Press Ctrl-C to quit." );
            }
            catch( Exception ex )
            {
                Console.Error.WriteLine( "Failed to start bot. Reason: {0}", ex.Message );
                Console.Error.Flush();
            }
        }

        private static void CommandLoop()
        {
            try
            {
                while( Running )
                {
                    string command = Console.ReadLine().Trim();

                    try
                    {
                        Command.Dispatch( command );
                    }
                    catch( Exception ex )
                    {
                        Console.Error.WriteLine( "Error processing command. Reason: {0}", ex.Message );
                        Console.Error.Flush();
                    }

                }
            }
            catch( ThreadAbortException ) { }
        }

        private static void ActionLoop( object param )
        {
            try
            {
                var action = param as Action;

                while( Running )
                {
                    try
                    {
                        action.Run();
                    }
                    catch( System.Net.WebException wex )
                    {
                        Console.Error.WriteLine( "Unable to complete request. Reason: {0}", wex.Message );
                        Console.Error.Flush();
                    }
                    catch( Exception ex )
                    {
#if DEBUG
                        Console.Error.WriteLine( ex.ToString() );
                        Console.Error.Flush();
#else
                        Console.Error.WriteLine( "Error in {0}. Reason: {1}", action.Name, ex.Message );
                        Console.Error.Flush();
#endif
                    }
                    finally
                    {
                        Thread.Sleep( action.Interval );
                    }
                }
            }
            catch( ThreadAbortException ) { }
        }

        private static void Prompt( string message, out string value )
        {
            Console.Write( message + ": " );
            value = Console.ReadLine();
        }

        //Command handlers

        private static void StatusCommand( params string[] args )
        {
            int longestName = ThreadManager.Threads
                .Select( x => x.Name )
                .OrderByDescending( x => x.Length )
                .First()
                .Length + 2;

            int longestStatus = ThreadManager.Threads
                .Select( x => x.ThreadState.Display() )
                .OrderByDescending( x => x.Length )
                .First()
                .Length + 2;

            longestName = Math.Max( longestName, 11 );
            longestStatus = Math.Max( longestStatus, 13 );

            var builder = new StringBuilder();
            builder.AppendFormat( "+{0}+{1}+", new String( '-', longestName ), new String( '-', longestStatus ) );
            builder.AppendLine();
            builder.AppendFormat( "|{0}|{1}|", " Task ID".PadRight( longestName ), " Task status".PadRight( longestStatus ) );
            builder.AppendLine();
            builder.AppendFormat( "+{0}+{1}+", new String( '-', longestName ), new String( '-', longestStatus ) );
            builder.AppendLine();

            foreach( var thread in ThreadManager.Threads )
            {
                builder.AppendFormat( "| {0} | {1} |", thread.Name.PadRight( longestName - 2 ), thread.ThreadState.Display().PadRight( longestStatus - 2 ) );
                builder.AppendLine();
            }

            builder.AppendFormat( "+{0}+{1}+", new String( '-', longestName ), new String( '-', longestStatus ) );
            Console.WriteLine( builder.ToString() );
        }
    }
}