﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace Warbot
{
    public sealed class CliOptions
    {
        [Option( 'u', "username", Required = false, HelpText = "The username of the bot account." )]
        public string Username { get; private set; }

        [Option( 'p', "password", Required = false, HelpText = "The bot's password as a plain string." )]
        public string Password { get; private set; }

        public void UnsetPassword()
        {
            this.Password = null;
        }

        [Option( 's', "subreddit", Required = false, HelpText = "Specify which subreddit to monitor (without leading /r/)." )]
        public string Subreddit { get; private set; }
    }
}