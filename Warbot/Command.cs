﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Warbot
{
    public delegate void CommandHandler( params string[] args );

    public abstract class Command
    {
        private static readonly Dictionary<string, CommandHandler> Commands;
        private static readonly string[] EmptyArgs;

        static Command()
        {
            Commands = new Dictionary<string, CommandHandler>();
            EmptyArgs = new String[0];
        }

        public static void Register( string name, CommandHandler handler )
        {
            name = name.ToLower();

            if( Commands.ContainsKey( name ) )
                return;

            Commands.Add( name, handler );
        }

        public static void Dispatch( string commandText )
        {
            var tokens = Parse( commandText );

            if( tokens.Count == 0 )
                return;

            var command = tokens.First();

            CommandHandler handler;
            if( !Commands.TryGetValue( command.ToLower(), out handler ) )
                throw new Exception( String.Format( "Unknown command '{0}'", command ) );

            handler( tokens.Count > 1 ? tokens.Skip( 1 ).ToArray() : EmptyArgs );
        }

        private static List<string> Parse( string commandText )
        {
            var tokens = new List<string>();
            int i = 0, len = commandText.Length;

            while( i < len )
            {
                while( i < len && Char.IsWhiteSpace( commandText[i] ) )
                    ++i;

                if( i >= len )
                    break;

                if( Char.IsLetterOrDigit( commandText[i] ) )
                {
                    var result = String.Empty;
                    while( i < len && Char.IsLetterOrDigit( commandText[i] ) )
                    {
                        result += commandText[i];
                        ++i;
                    }

                    tokens.Add( result );
                    continue;
                }

                if( commandText[i] == '"' )
                {
                    ++i;
                    var result = String.Empty;
                    while( i < len && commandText[i] != '"' )
                    {
                        if( commandText[i] == '\\' )
                        {
                            ++i;
                            result += commandText[i];
                            ++i;
                        }
                        else
                        {
                            result += commandText[i];
                            ++i;
                        }
                    }

                    tokens.Add( result );
                    continue;
                }

                throw new Exception( String.Format( "Bad command string. Unexpected character '{0}' (0x{1:X2}) at position {2}", commandText[i], Convert.ToInt16( commandText[i] ), i + 1 ) );
            }

            return tokens;
        }
    }
}