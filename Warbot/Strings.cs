﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;

namespace Warbot
{
    public static class Strings
    {
        private static readonly Regex ReplacerRegex;

        static Strings()
        {
            ReplacerRegex = new Regex( @"[^\\]#\{(?<Name>\w+)\}", RegexOptions.Compiled );
        }

        public static string Get( string name, Dictionary<string, object> parameters )
        {
            var asm = Assembly.GetExecutingAssembly();
            var path = String.Format( "Warbot.Strings.{0}.txt", name );
            var result = (string)null;

            using( var stream = asm.GetManifestResourceStream( path ) )
            using( var reader = new StreamReader( stream, Encoding.UTF8, true, 1024 * 8, false ) )
            {
                result = reader.ReadToEnd();
            }

            MatchEvaluator replacer = delegate( Match match )
            {
                var key = match.Groups["Name"].Value;
                var value = "<null>";

                if( parameters.ContainsKey( key, false, out key ) )
                    value = parameters[key].ToString();

                return match.Value.Substring( 0, match.Value.IndexOf( '#' ) ) + value;
            };

            return ReplacerRegex.Replace( result, replacer );
        }
    }
}