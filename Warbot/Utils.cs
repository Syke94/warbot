﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warbot
{
    internal static class Utils
    {
        public static int CompareVersions( string first, string second )
        {
            if( first == second || first == null || second == null )
                return 0;

            Func<string, int> Parse = delegate( string input )
            {
                var chars = input.ToCharArray();

                if( chars.All( Char.IsDigit ) )
                    return Int32.Parse( input );
                else
                    return input.ToCharArray().Select( Convert.ToInt32 ).Sum();
            };

            var i = 1000000;
            var j = i;
            var a = first.Split( '.' ).Select( Parse ).Sum( x => x * ( i /= 10 ) );
            var b = second.Split( '.' ).Select( Parse ).Sum( x => x * ( j /= 10 ) );

            return a - b;
        }
    }
}