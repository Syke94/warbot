﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedditSharp;
using RedditSharp.Things;
using HtmlAgilityPack;
using System.Net;
using System.Text.RegularExpressions;

namespace Warbot.Actions
{
    internal sealed class ScanVersionsAction : Action
    {
        private const string ForumsUrl = "https://forums.warframe.com/";
        private readonly Regex PcMarkdownRegex = new Regex( @"####\[(?<Version>.*?)\]\((?<Url>.*?)\) \[\*\*PC\*\*\] \|", RegexOptions.IgnoreCase | RegexOptions.Compiled );
        private readonly Regex ConsoleMarkdownRegex = new Regex( @"\| \[(?<Version>.*?)\]\((?<Url>.*?)\) \[~~PS4\/XB1~~\]", RegexOptions.IgnoreCase | RegexOptions.Compiled );

        public ScanVersionsAction() : base( "version" )
        {
            this.Interval = 1.Hours();
        }

        public override void Run()
        {
            string html;

            using( var client = new WebClient() )
            {
                html = client.DownloadString( ForumsUrl );
            }

            var document = new HtmlDocument();
            document.LoadHtml( html );

            var forumVersions = document.GetElementbyId( "top-links" )
                                        .ChildNodes
                                        .Skip( 1 )
                                        .First()
                                        .ChildNodes
                                        .Skip( 3 )
                                        .First()
                                        .ChildNodes
                                        .Where( x => x.Name.ToLower() == "td" )
                                        .Select( delegate( HtmlNode node )
                                        {
                                            var platform = node.ChildNodes[0].Attributes["alt"].Value;
                                            var anchor = node.ChildNodes[2];
                                            var url = anchor.Attributes["href"].Value;
                                            var version = anchor.InnerText;

                                            return new { Platform = platform, URL = url, Version = version };
                                        } );

            var pcVersion = forumVersions.Where( x => x.Platform == "PC" ).First();
            var ps4Version = forumVersions.Where( x => x.Platform == "PS4" ).First();
            var xb1Version = forumVersions.Where( x => x.Platform == "XB1" ).First();

            if( Utils.CompareVersions( ps4Version.Version, xb1Version.Version ) != 0 )
            {
                Console.WriteLine( "[Version] :: PS4 and Xbox One versions do not match. Skipping." );
                return;
            }

            var markdown = Program.Subreddit.Description;
            var currentPcVersion = PcMarkdownRegex.Match( markdown );
            var currentConsoleVersion = ConsoleMarkdownRegex.Match( markdown );

            if( !currentConsoleVersion.Success || !currentPcVersion.Success )
            {
                Console.WriteLine( "[Version] :: Version numbers could not be parsed from markdown." );
                return;
            }

            var updated = false;

            if( Utils.CompareVersions( currentPcVersion.Groups["Version"].Value, pcVersion.Version ) < 0 )
            {
                Console.WriteLine( "[Version] :: Current PC version is outdated. Current: {0}, New: {1}", currentPcVersion.Groups["Version"].Value, pcVersion.Version );
                markdown = PcMarkdownRegex.Replace( markdown, ( _ ) => String.Format( "####[{0}]({1}) [**PC**] | ", pcVersion.Version, pcVersion.URL ) );
                updated = true;
            }

            if( Utils.CompareVersions( currentConsoleVersion.Groups["Version"].Value, ps4Version.Version ) < 0 )
            {
                Console.WriteLine( "[Version] :: Current console version is outdated. Current: {0}, New: {1}", currentConsoleVersion.Groups["Version"].Value, ps4Version.Version );
                markdown = PcMarkdownRegex.Replace( markdown, ( _ ) => String.Format( "| [{0}]({1}) [PS4/XB1]", ps4Version.Version, ps4Version.URL ) );
                updated = true;
            }

            var parameters = new Dictionary<string, object>
            {
                { "CURRENT_PC", currentPcVersion.Groups["Version"].Value },
                { "CURRENT_PC_LINK", currentPcVersion.Groups["Url"].Value },
                { "NEW_PC", pcVersion.Version },
                { "NEW_PC_LINK", pcVersion.URL },

                { "CURRENT_CONSOLE", currentConsoleVersion.Groups["Version"].Value },
                { "CURRENT_CONSOLE_LINK", currentConsoleVersion.Groups["Url"].Value },
                { "NEW_CONSOLE", ps4Version.Version },
                { "NEW_CONSOLE_LINK", ps4Version.URL },
            };

            var modmail = Strings.Get( "VersionUpdateModmail", parameters );

            Console.WriteLine( markdown == Program.Subreddit.Settings.Sidebar );

            //NOTE: for some reason 'UpdateSettings' causes some things
            //      to be changed erroneously. This includes spam filter strengths
            //      this is a problem with RedditSharp sending incorrect JSON to reddit's API
            //      until this issue is sorted out, actually updating settings is disabled.

            if( false )
            {
                Program.Subreddit.Settings.Sidebar = markdown;
                Program.Subreddit.Settings.UpdateSettings();
                Program.Reddit.ComposePrivateMessage( "Versions updated.", modmail, String.Format( "/r/{0}", Program.Subreddit.DisplayName ) );
                Console.WriteLine( "[Version] :: Version numbers successfully updated." );
            }
            else
            {
                Console.WriteLine( "[Version] :: New versions are available. PC: {0}/{1}, Console: {2}/{3}.",
                    currentPcVersion.Groups["Version"].Value, pcVersion.Version,
                    currentConsoleVersion.Groups["Version"].Value, ps4Version.Version );
            }
        }
    }
}