﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RedditSharp;
using RedditSharp.Things;
using System.Text.RegularExpressions;

namespace Warbot.Actions
{
    internal sealed class FilterModQueueAction : Action
    {
        private readonly Dictionary<string, object> Parameters;
        private readonly Regex ShortlinkRegex;
        private bool FirstRun;

        public FilterModQueueAction() : base( "modqueue" )
        {
            this.Interval = 15.Seconds();
            this.FirstRun = true;
            this.ShortlinkRegex = new Regex( @"https?://(?<Head>ow|bit|tinyurl|goo|t|adf)\.(?<Tail>com?|ly|gl)", RegexOptions.Compiled | RegexOptions.IgnoreCase );
            this.Parameters = new Dictionary<string, object>
            {
                { "Author", null },
                { "Type", null },
                { "Article", null },
                { "Domain", null },
                { "Subreddit", null }
            };
        }

        public override void Run()
        {
            if( this.FirstRun )
            {
                this.FirstRun = false;
                this.Parameters["Subreddit"] = Program.Subreddit.DisplayName;
            }

            var queue = Program.Subreddit.ModQueue;
            
            foreach( var item in queue )
            {
                if( item is Post )
                {
                    var post = item as Post;

                    //No need to police linkposts, Reddit won't even allow
                    //linkposts with short urls to be submittted.
                    if( !post.IsSelfPost )
                        continue;

#if !DEBUG
                    //Moderators and things that have already been approved are exempt from being removed.
                    if( post.ApprovedBy != null || Program.Subreddit.Moderators.Any( x => x.Name == post.AuthorName )
                        continue;
#endif
                    var match = this.ShortlinkRegex.Match( post.SelfText );

                    if( !match.Success )
                        continue;

                    this.Parameters["Type"] = "post";
                    this.Parameters["Author"] = post.AuthorName;
                    this.Parameters["Domain"] = String.Format( "{0}.{1}", match.Groups["Head"].Value, match.Groups["Tail"].Value );
                    this.Parameters["Article"] = match.Groups["Head"].Value[0].IsVowel() ? "an" : "a";

                    var response = Strings.Get( "ShortLinkRemoved", this.Parameters );
                    post.Comment( response ).Distinguish( DistinguishType.Moderator );
                    post.Remove();

                    Console.WriteLine( "[ModQueue] :: Took down post by {0} at {1}", post.AuthorName, post.Shortlink );
                }

                if( item is Comment )
                {
                    var comment = item as Comment;

#if !DEBUG
                    //ditto
                    if( comment.ApprovedBy != null || Program.Subreddit.Moderators.Any( x => x.Name == comment.Author ) )
                        continue;
#endif

                    var match = this.ShortlinkRegex.Match( comment.Body );

                    if( !match.Success )
                        continue;

                    this.Parameters["Type"] = "comment";
                    this.Parameters["Author"] = comment.Author;
                    this.Parameters["Domain"] = String.Format( "{0}.{1}", match.Groups["Head"].Value, match.Groups["Tail"].Value );
                    this.Parameters["Article"] = match.Groups["Head"].Value[0].IsVowel() ? "an" : "a";

                    var response = Strings.Get( "ShortLinkRemoved", this.Parameters );
                    comment.Reply( response ).Distinguish( DistinguishType.Moderator );
                    comment.Remove();

                    Console.WriteLine( "[ModQueue] :: Took down comment by {0} at {1}", comment.Author, comment.Shortlink );
                }
            }
        }
    }
}