﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Warbot.Actions
{
    internal abstract class Action
    {
        public int Interval { get; protected set; }
        public string Id { get; private set; }

        protected Action( string id )
        {
            this.Id = id;
        }

        public abstract void Run();
    }
}