Prerequisites
=============

* None!

Using the bot
=============

To use the bot, open up the solution in Visual Studio or build with MSBuild, then navigate to `Bin\Release` and run `Warbot.exe` from the command line.

You can specify your username, password, and target subreddit via the command line:

    Command line options:

      -u, --username <username>   - Your username as a string. Without leading /u/
      -p, --password <password>   - Your password as a string.
      -s, --subreddit <subreddit> - Target subreddit as a string. Without leading /r/

      Example:
        $ Warbot.exe -u CoolMod77 -p super_secret123 -s AskReddit